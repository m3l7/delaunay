# %%
import numpy as np
import math
from abc import ABC, abstractmethod

# %% base abstract triangulation class
class Triangulation(ABC):
    def __init__(self):
        self.precision = 0.000001

        self.triangles = []
        self.points = []
        self.edges = []

        super().__init__()

    @abstractmethod
    def addPoint(self,x,y):
        pass

    # print a matlab script for plotting the triangulation
    def printMatlabScript(self):
        print("p = transpose([", end='')
        for point in self.points:
            print(str(point[0]) + " " + str(point[1]), end='; ')
        print("])")

        print("t = [", end='')
        for triangle in self.triangles:
            print(str(triangle[0] + 1) + " " + str(triangle[1] + 1) + " " + str(triangle[2] + 1), end='; ')
        print("]")
        print("triplot(t, p(1,:), p(2,:))")
        

    # compute angular coefficient and intercept of a segment
    def segmentCoeff(self, segment):
        point1 = segment[0]
        point2 = segment[1]

        m = (point2[1] - point1[1]) / (point2[0] - point1[0])
        q = point1[1] - m * point1[0]

        return m, q

    # compute the x,y intersection of two segments
    def segmentIntersection(self, segment1, segment2):

        if (segment1[0][0] == segment1[1][0] and segment2[0][0] == segment2[1][0]):
            # parallel segments
            return [float('nan'), float('nan')]
        elif segment1[0][0] == segment1[1][0]:
            m2, q2 = self.segmentCoeff(segment2)
            return [segment1[0][0], m2*segment1[0][0] + q2]
        elif segment2[0][0] == segment2[1][0]:
            m1, q1 = self.segmentCoeff(segment1)
            return [segment2[0][0], m1*segment2[0][0] + q1]


        # compute the angular coeff and intercept of the two segments
        m1, q1 = self.segmentCoeff(segment1)
        m2, q2 = self.segmentCoeff(segment2)

        if m1 == m2:
            # parallel segments
            return [float('nan'), float('nan')]

        x = - (q2 - q1) / (m2 - m1)
        y = m1*x + q1

        return [x,y]

    # check if a point of a line lies inside a portion (segment) of the line
    def isPointInsideSegment(self, point, segment):
        point_x = point[0]
        point_y = point[1]
        segment_A = segment[0]
        segment_B = segment[1]

        if (segment_A[0] == segment_B[0]):
            # compare y coordinates
            if (point_y > segment_A[1] + self.precision and point_y < segment_B[1] - self.precision) or (point_y < segment_A[1] - self.precision and point_y > segment_B[1] + self.precision):
                return True
        else:
            # compare x coordinates
            if (point_x >= (segment_A[0] + self.precision) and point_x <= (segment_B[0]) - self.precision) or (point_x <= (segment_A[0] - self.precision) and (point_x >= segment_B[0] + self.precision)):
                return True

        return False

    def isPointHiddenBySegment(self, pointIdx, coveringSegment, hiddenPoint):
        # compute intersection between the two segments
        point = self.points[pointIdx]
        PH = [point, hiddenPoint]
        intersection = self.segmentIntersection(coveringSegment, PH)

        if math.isnan(intersection[0]):
            return False

        # the point is hidden if the intersection is inside both segments
        if self.isPointInsideSegment(intersection, PH) and self.isPointInsideSegment(intersection, coveringSegment):
            return True
        else:
            return False
        

    # compute the intersection of two segments lying on the same line
    def TwoSegmentsIntersection(self, segment1, segment2):
        segment1A = segment1[0]
        segment1B = segment1[1]
        segment2A = segment2[0]
        segment2B = segment2[1]
        if segment1A[0] < segment1B[0] or (segment1A[0] == segment1B[0] and segment1A[1] < segment1B[1]):
            min_1 = segment1A
            max_1 = segment1B
        else:
            min_1 = segment1B
            max_1 = segment1A
        if segment2A[0] < segment2B[0] or (segment2A[0] == segment2B[0] and segment2A[1] < segment2B[1]):
            min_2 = segment2A
            max_2 = segment2B
        else:
            min_2 = segment2B
            max_2 = segment2A

        if min_1[0] < min_2[0] or (min_1[0] == min_2[0] and min_1[1] < min_2[1]):
            int_A = min_2
        else:
            int_A = min_1
        if max_1[0] < max_2[0] or (max_1[0] == max_2[0] and max_1[1] < max_2[1]):
            int_B = max_1
        else:
            int_B = max_2
        
        return [int_A, int_B]

    # check if a segment is covering another segment, from the input point perspective
    def isSegmentHiddenBySegment(self, pointIdx, hiddenSegment, coveringSegment):

        # check wheter one of the two vertices are hidden by the segment
        if self.isPointHiddenBySegment(pointIdx, coveringSegment, hiddenSegment[0]) or self.isPointHiddenBySegment(pointIdx, coveringSegment, hiddenSegment[1]):
            return True

        # let the covering segment be AB and the input point P.
        # compute intersection of hidden segment and PA and PB
        point = self.points[pointIdx]
        PA = [point, coveringSegment[0]]
        PB = [point, coveringSegment[1]]
        int_1 = self.segmentIntersection(PA, hiddenSegment)
        int_2 = self.segmentIntersection(PB, hiddenSegment)

        # compute the portion of the intersection inside the input segment
        hiddenSegment_int = self.TwoSegmentsIntersection(hiddenSegment, [int_1, int_2])
        hiddenMidpoint = [(hiddenSegment_int[0][0] + hiddenSegment_int[1][0]) / 2, (hiddenSegment_int[0][1] + hiddenSegment_int[1][1])/2]

        # check if the points inside the portion are hidden by the covering segment
        if np.isclose(hiddenSegment_int[0][0], hiddenSegment_int[1][0]) and np.isclose(hiddenSegment_int[0][1], hiddenSegment_int[1][1]):
            return False
        return self.isPointHiddenBySegment(pointIdx, coveringSegment, hiddenMidpoint)


    # check if a segment is seen from a point
    def isSegmentSeen(self, segment, pointIdx):
        hidden = False
        for loopSegment in self.edges:

            # skip in case we are analyzing two identical segments
            if (segment == loopSegment):
                continue

            segmentCoords = [self.points[segment[0]], self.points[segment[1]]]
            loopSegmentCoords = [self.points[loopSegment[0]], self.points[loopSegment[1]]]
            if (self.isSegmentHiddenBySegment(pointIdx, segmentCoords, loopSegmentCoords)):
                hidden = True
                break
        return not hidden
        
        
    # find the edges that the point "sees" (see prep. 6.2)
    def seenEdges(self, pointIdx):
        seenEdges = []
        for segment in self.edges:
            if self.isSegmentSeen(segment, pointIdx):
                seenEdges.append(segment)

        return seenEdges

    # check if a triangle is stored in the instance triangles
    def isTriangleStored(self, triangle):
        if len(triangle) == 3:
            for storedTriangle in self.triangles:
                if (triangle[0] in storedTriangle) and (triangle[1] in storedTriangle) and (triangle[2] in storedTriangle):
                    return True
        
        return False

    # remove a triangle from the instance triangles
    def removeTriangle(self, triangle):
        for storedTriangle in self.triangles:
            if (triangle[0] in storedTriangle) and (triangle[1] in storedTriangle) and (triangle[2] in storedTriangle):
                self.triangles.remove(storedTriangle)
        

class ScanTriangulation(Triangulation):
    def addPoint(self,point):

        self.points.append(point)
        pointIdx = len(self.points) - 1

        if len(self.points) == 3:
            self.triangles = [[0,1,2]]
            self.edges = [[0,1], [1,2], [2,0]]
        elif len(self.points) > 3:
            # compute the portion of the external contour to link with the new point
            seenEdges = self.seenEdges(pointIdx)
            for segment in seenEdges:
                # add a new triangle
                self.triangles.append([segment[0], segment[1], pointIdx])

                for point in segment:
                    if ([pointIdx, point] not in self.edges) and ([point, pointIdx] not in self.edges):
                        self.edges.append([pointIdx, point])

            # check if the added point is inside a triangle (refinement)
            if len(seenEdges) == 3:
                uniqueSeenPoints = list(set().union(seenEdges[0], seenEdges[1], seenEdges[2]))
                if self.isTriangleStored(uniqueSeenPoints):
                    # remove the triangle and keep the refinement
                    self.removeTriangle(uniqueSeenPoints)


class Delaunay(ScanTriangulation):
    edgesToCheck = []

    # compute the triangles which has the specific edge
    def computeEdgeTriangles(self, edge):
        ret = []
        for triangle in self.triangles:
            if (edge[0] in triangle) and (edge[1] in triangle):
                ret.append(triangle)
         
        return ret

    # check if 3 points are in counter clockwise order
    def ccw (self, point1, point2, point3):
        return (point2[0] - point1[0])*(point3[1] - point1[1])-(point3[0] - point1[0])*(point2[1] - point1[1]) > 0

    # check if a point lies in the excircle of a triangle
    def isPointInsideTriangleCircumcircle(self, point, triangle):

        a = self.points[triangle[0]]
        b = self.points[triangle[1]]
        c = self.points[triangle[2]]
        p = self.points[point]

        # check if the triangles points are clockwise or anticlockwise
        if not self.ccw(a, b, c):
            t = b
            b = c
            c = t

        dx = a[0] - p[0]
        dy = a[1] - p[1]
        ex = b[0] - p[0]
        ey = b[1] - p[1]
        fx = c[0] - p[0]
        fy = c[1] - p[1]

        ap = dx * dx + dy * dy
        bp = ex * ex + ey * ey
        cp = fx * fx + fy * fy

        # compute the determinant to determine if the point lies inside circumcircle
        return (dx * (ey * cp - bp * fy) -
                dy * (ex * cp - bp * fx) +
                ap * (ex * fy - ey * fx)) > 0.0

    # Return the cross product AB x BC.
    # The cross product is a vector perpendicular to AB
    # and BC having length |AB| * |BC| * Sin(theta) and
    # with direction given by the right-hand rule.
    # For two vectors in the X-Y plane, the result is a
    # vector with X and Y components 0 so the Z component
    # gives the vector's length and direction.
    def CrossProductLength(self, Ax, Ay, Bx, By, Cx, Cy):
        # Get the vectors' coordinates.
        BAx = Ax - Bx
        BAy = Ay - By
        BCx = Cx - Bx
        BCy = Cy - By

        # Calculate the Z coordinate of the cross product.
        return (BAx * BCy - BAy * BCx)
    
    # check if 2 neighbour triangles are in convex position
    def checkTrianglesConvexity(self, commonEdge, triangle1, triangle2):

            # compute external points in order
            uniquePoints = []
            for point1 in triangle1:
                if point1 not in triangle2:
                    uniquePoints.append(point1)

            uniquePoints.append(commonEdge[0])

            for point2 in triangle2:
                if point2 not in triangle1:
                    uniquePoints.append(point2)
                    
            uniquePoints.append(commonEdge[1])

            # uniquePoints = list(set().union(triangle1, triangle2))
            points = []
            for uniquePoint in uniquePoints:
                points.append(self.points[uniquePoint])

            # For each set of three adjacent points A, B, C,
            # find the cross product AB · BC. If the sign of
            # all the cross products is the same, the angles
            # are all positive or negative (depending on the
            # order in which we visit them) so the polygon
            # is convex.
            got_negative = False
            got_positive = False
            for A in range(len(points)):
                B = (A + 1) % 4
                C = (B + 1) % 4

                cross_product = self.CrossProductLength(points[A][0], points[A][1], points[B][0], points[B][1], points[C][0], points[C][1])
                if cross_product < 0:
                    got_negative = True
                elif cross_product > 0:
                    got_positive = True
                if got_negative and got_positive:
                    return False

            # If we got this far, the polygon is convex.
            return True

    # check if 2 neighbour triangles satisfy the Delaunay triangulation
    def checkTrianglesDelaunayValidity(self, edge, triangle1, triangle2):

        if not self.checkTrianglesConvexity(edge, triangle1, triangle2):
            return True

        # compute the point of the second triangle not in common with the first
        point4 = None
        for point in triangle2:
            if point not in triangle1:
                point4 = point
                break

        return not self.isPointInsideTriangleCircumcircle(point4, triangle1)

    # compute the swapped triangles, common edge and external edges from the old ones
    def computeSwappedEdgeTriangles(self, edge, triangle1, triangle2):
        newEdge = []
        for point in (triangle1 + triangle2):
            if point not in edge:
                newEdge.append(point)

        newTriangle1 = [newEdge[0], newEdge[1], edge[0]]
        newTriangle2 = [newEdge[0], newEdge[1], edge[1]]

        externalEdge1 = [edge[0], newEdge[0]]
        externalEdge2 = [newEdge[0], edge[1]]
        externalEdge3 = [edge[1], newEdge[1]]

        return newEdge, [newTriangle1, newTriangle2], [externalEdge1, externalEdge2, externalEdge3]

    # remove triangles from the instance triangle list
    # warning: does not check for points order.
    # works only if the triangle is taken directly from the triangles list
    def removeTriangles(self, triangles):
        for triangleToRemove in triangles:
            for triangle in self.triangles:
                if (triangleToRemove[0] in triangle) and (triangleToRemove[1] in triangle) and (triangleToRemove[2] in triangle):
                    self.triangles.remove(triangle)
    
    # remove edges from the instance segments list
    def removeEdges(self, edges):
        for edgeToRemove in edges:
            for edge in self.edges:
                if (edgeToRemove[0] in edge) and (edgeToRemove[1] in edge):
                    self.edges.remove(edge)

    # loop through all edges and do edge swapping
    # until all edges satisfy Delaunay condition
    def edgesSwap(self):

        self.edgesToCheck = self.edges.copy()

        while len(self.edgesToCheck) > 0:
            edge = self.edgesToCheck[0].copy()

            # compute the triangles that own this edge (at most 2)
            triangles = self.computeEdgeTriangles(edge)

            # check if the edge is a boundary edge
            if len(triangles) != 2:
                self.edgesToCheck.pop(0)
                continue

            if not self.checkTrianglesDelaunayValidity(edge, triangles[0], triangles[1]):
                # do edge swap
                newEdge, newTriangles, newExternalEdges = self.computeSwappedEdgeTriangles(edge, triangles[0], triangles[1])

                # replace old triangles and edge with new ones in the class list
                self.removeTriangles(triangles)
                self.removeTriangles(newTriangles)
                self.removeEdges([edge, newEdge])
                self.triangles.append(newTriangles[0])
                self.triangles.append(newTriangles[1])
                self.edges.append(newEdge)

                # mark the neighors edges as edges to check
                for e in newExternalEdges:
                    if ([e[0], e[1]] not in self.edgesToCheck) and ([e[1], e[0]] not in self.edgesToCheck):
                        self.edgesToCheck.append(e)
                self.edgesToCheck.append(newEdge)

            self.edgesToCheck.pop(0)