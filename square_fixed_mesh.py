from delaunay import Delaunay
import numpy

delaunay = Delaunay()

for i in range(5):
    for j in range(5):
        print("Adding point" + str(i) + " " + str(j))
        dx = numpy.random.uniform(-0.01,0.01)
        dy = numpy.random.uniform(-0.01,0.01)
        delaunay.addPoint([i + dx, j + dy])

delaunay.edgesSwap()
delaunay.printMatlabScript()