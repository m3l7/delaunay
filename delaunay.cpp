#include <iostream>
#include <fstream>
#include <vector>
#include <stack>
#include <algorithm>
#include <math.h>

using namespace std;

//structure representing a 2d point
struct Point {

    double x,y;

    /*Compare two point instances 
      Two points are sorted first by x-coordinate 
      and if x-coordinates are equal then by y-coordinate 
    */
    bool operator <(const Point &p) const {
        return x < p.x || (x == p.x && y < p.y);
    }
};

//structure representing a triangle
struct Triangle {

    //vertices of the triangle
    vector<int> points;
};

//structure representing an edge between two points
struct Edge {
    
    //start point of edge
    size_t p1;

    //end point of edge
    size_t p2;
    
    //triangles that are next to the edge
    vector<size_t> triangles = {};

    //edge is marked. this used in edge flip algorithm.
    bool mark = true;

    //compare two edges
    //edges are equal, if they have same end points. direction of edge does not matter.
    //equal edges can have different triangles next to them
    bool operator ==(const Edge &p) const {
        return (p1 == p.p1 && p2 == p.p2) || (p1 == p.p2 && p2 == p.p1);  
    }
};

//the collection of all triangles in the triangulation
vector<Triangle> triangles = {};

//the collection of all points in the triangulated mesh
vector<Point> coords;

//the collection of all edges in the triangulation
vector<Edge> edges = {};

//compute the cross product between two given points
double cross(const Point &O, const Point &A, const Point &B) {
    return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
}

//compute the centroid of given triangle
Point find_centroid(size_t triangle_num) {
    double x = 0;
    double y = 0;

    //sum all the x and y coords of triangle's vertices
    for(int i = 0; i < 3; i++) {
        x += coords[triangles[triangle_num].points[i]].x;
        y += coords[triangles[triangle_num].points[i]].y;
    }

    Point center;

    //Take the averages of the triangle's vertices coordinates
    center.x = x / 3;
    center.y = y / 3;

    return center;
}

//compares two points of a given triangle to sort them in counterclockwise order
struct triangle_comp {

    //number of triangle to use for reference
    int triangle_num;

    //compare two triangle vertices
    //sorts the points in counterclockwise order
    bool operator() (int p1i, int p2i) {
        Point centroid = find_centroid(triangle_num);

        Point p1 = coords[p1i];
        Point p2 = coords[p2i];

        double a1 = (atan2(p1.y - centroid.y, p1.x - centroid.x) * 180/3.14159);
        double a2 = (atan2(p2.y - centroid.y, p2.x - centroid.x) * 180/3.14159);

        return a1 < a2;
    }
};

//check if given point p lies inside the circumcirle of triangle (a,b,c)
bool in_circle(Point a, Point b, Point c, Point p) {
    double dx = a.x - p.x;
    double dy = a.y - p.y;
    double ex = b.x - p.x;
    double ey = b.y - p.y;
    double fx = c.x - p.x;
    double fy = c.y - p.y;

    double ap = dx * dx + dy * dy;
    double bp = ex * ex + ey * ey;
    double cp = fx * fx + fy * fy;

    //compute the determinant to determine if p lies inside circumcircle
    return (dx * (ey * cp - bp * fy) -
            dy * (ex * cp - bp * fx) +
            ap * (ex * fy - ey * fx)) > 0.0;
}

//find the starting point of new edge to be added when doing edge flip
int find_fourth_point(int p1, int p2, int p3, Edge e) {
    int p5 = -1;
    if(p1 != e.p1 && p1 != e.p2) {
        p5 = p1;
    }
           
    if(p2 != e.p1 && p2 != e.p2) {
        p5 = p2;
    }
            
    if(p3 != e.p1 && p3 != e.p2) {
        p5 = p3;
    }
    return p5;
}

//find the ending point of new edge to be added when doing edge flip
int find_third_point(int p1, int p2, int p3, int triangle_num) {
    int p4 = -1;
    for(int i = 0; i < 3; i++) {
        if((triangles[triangle_num].points[i] != p1) && (triangles[triangle_num].points[i] != p2) && (triangles[triangle_num].points[i] != p3)) {
            p4 = triangles[triangle_num].points[i];
            break;
        }
    }
    return p4;
}

//add a new edge to collection of edges
void add_edge(int p1, int p2, int triangle_num) {
    vector<Edge>::iterator temp;

    //construct the new edge
    Edge e;
    e.p1 = p1;
    e.p2 = p2;
    e.triangles.push_back(triangle_num);

    //search for existing edge with same endpoints
    if((temp = find(edges.begin(), edges.end(), e)) != edges.end()) {
        //if found, just add the given triangle as neighbor of the edge
        temp -> triangles.push_back(triangle_num);
    } else {
        //otherwise add the new edge to edges collection
        edges.push_back(e);
    }
}       

//replace given triangle with the edge flipped one
void replace_triangle(int triangle_num, int triangle_other, Edge *e, int p) {
    //get the vertices of the old triangle
    int p1 = triangles[triangle_num].points[0];
    int p2 = triangles[triangle_num].points[1];
    int p3 = triangles[triangle_num].points[2];
            
    //get the start and end point of the new edge
    int p4 = find_third_point(p1, p2, p3, triangle_other);
    int p5 = find_fourth_point(p1, p2, p3, *e);

    //sort the points of the new triangle in counterclockwise order
    vector<int> sorted_points = {p, p4, p5};
    triangle_comp tc;
    tc.triangle_num = triangle_num;
    sort(sorted_points.begin(), sorted_points.end(), tc);

    //replace the old triangle with the new one
    triangles[triangle_num].points[0] = sorted_points[0];
    triangles[triangle_num].points[1] = sorted_points[1];
    triangles[triangle_num].points[2] = sorted_points[2];

    //add the new edge to edges collection
    add_edge(p4, p5, triangle_num);
}

//load points to triangulate from given file
void load_points_from_file(string filename) {

    //open the file with given name
    ifstream source;
    source.open(filename);

    vector<string> lines;
    string line;

    //read coordinates from the file and add the to points collection
    while(source.good()) {
        Point p;
        getline(source, line, ',');

        if(line == "")
            break;

        p.x = stod(line);
        getline(source, line, '\n');
        p.y = stod(line);
        
        coords.push_back(p);
    }

    source.close();
}

//do initial triangulation of the given point mesh
void do_basic_triangulation() {
    size_t n = coords.size();
    size_t k = 0;

    vector<size_t> H(coords.size() * 2);

    for (size_t i = 0; i < n; i++) {
        while (k >= 2 && cross(coords[H[k-2]], coords[H[k-1]], coords[i]) <= 0) {
            Triangle t;
            t.points = vector<int>(3);
            t.points[0] = H[k-1];
            t.points[1] = H[k-2];
            t.points[2] = i;
            triangles.push_back(t);                   
            k--;
        }
        H[k++] = i;
    }

    for (size_t i = n-1, t = k+1; i > 0; i--) {
        while (k >= t && cross(coords[H[k-2]], coords[H[k-1]], coords[i-1]) <= 0) {      
            Triangle t;
            t.points = vector<int>(3);
            t.points[0] = H[k-1];
            t.points[1] = H[k-2];
            t.points[2] = i - 1;
            triangles.push_back(t);       
            k--;
        }
        H[k++] = i-1;
    }
}

//compute edges from the initial triangulation
void compute_edges() {
    for (size_t i = 0; i < triangles.size(); i++) {
        size_t p1 = triangles[i].points[0];
        size_t p2 = triangles[i].points[1];
        size_t p3 = triangles[i].points[2];
        
        add_edge(p1, p2, i);
        add_edge(p2, p3, i);
        add_edge(p3, p1, i);
    }
}

//store current triangulation into given file
void store_triangles(string filename) {
    ofstream dest;
    dest.open(filename);

    for(vector<Triangle>::iterator it = triangles.begin(); it != triangles.end(); ++it) {
        dest << it->points[0] << '\n';
        dest << it->points[1] << '\n';
        dest << it->points[2] << '\n';
    }
    dest.close();
}

//store points to given file
void store_points(string filename) {
    ofstream dest;
    dest.open(filename);

    for(vector<Point>::iterator it = coords.begin(); it != coords.end(); ++it) {
        dest << it->x << "," << it->y << '\n';
    }
    dest.close();
}

//initialize the edge stack to be used for edge flipping algorithm
stack<Edge*> initialize_edge_stack() {
    stack<Edge*> edge_stack;
    for(vector<Edge>::iterator it = edges.begin(); it != edges.end(); ++it) {
        edge_stack.push(&(*it));
    }
    return edge_stack;
}

//check if given triangle violates delaunay constraint
bool check_for_triangle_violation(int triangle_num, int triangle_other) {
    int p1 = triangles[triangle_num].points[0];
    int p2 = triangles[triangle_num].points[1];
    int p3 = triangles[triangle_num].points[2];
            
    int p4 = find_third_point(p1, p2, p3, triangle_other);
    
    return in_circle(coords[p1], coords[p2], coords[p3], coords[p4]);
}

//check if any of the triangles adjacent to given edge violates delaunay constraint
bool check_for_edge_violation(Edge *e) {
    int t1 = e->triangles[0];
    int t2 = e->triangles[1];

    bool violates = check_for_triangle_violation(t1, t2);
    violates = violates || check_for_triangle_violation(t2, t1);
    return violates;
}

//update adjacent triangles for a given edge
void update_edge(int p1, int p2, int triangle_num) {
    Edge e;
    e.p1 = p1;
    e.p2 = p2;
    vector<Edge>::iterator temp = find(edges.begin(), edges.end(), e);
    temp -> triangles.push_back(triangle_num);   
}

//clear the adjacent triangles for all edges
void clear_edges() {
    for(int i = 0; i < edges.size(); i++) {
        edges[i].triangles = {};
    }
}

//recompute the adjacent triangles for all edges
void update_triangle_edges() {
    clear_edges();
    for (size_t i = 0; i < triangles.size(); i++) {
        size_t p1 = triangles[i].points[0];
        size_t p2 = triangles[i].points[1];
        size_t p3 = triangles[i].points[2];
        
        update_edge(p1, p2, i);
        update_edge(p2, p3, i);
        update_edge(p3, p1, i);
    }
}

//push unmarked edges for flipped triangles to edge stack
void push_unmarked_edges(int t1, int t2, Edge* e, int p, stack<Edge*> edge_stack) {
    //take the vertices of the first triangle
    int p1 = triangles[t1].points[0];
    int p2 = triangles[t1].points[1];
    int p3 = triangles[t1].points[2];
            
    //compute the end points of the flipped edge
    int p4 = find_third_point(p1, p2, p3, t2);
    int p5 = find_fourth_point(p1, p2, p3, *e);

    //if edge p1 -> p4 is unamarked add it to top of stack
    Edge e2;
    e2.p1 = p1;
    e2.p2 = p4;
    vector<Edge>::iterator temp = find(edges.begin(), edges.end(), e2);
    if(temp -> mark == false) {
        edge_stack.push(&(*temp));
    }
    
    //if edge p1 -> p5 is unmarked add it to top of stack
    e2.p1 = p1;
    e2.p2 = p5;
    temp = find(edges.begin(), edges.end(), e2);
    if(temp -> mark == false) {
        edge_stack.push(&(*temp));
    }  
}

//execute the edge flipping algorithm
void do_edge_flip() {

    //create the edge stack used by edge flip algorithm
    stack<Edge*> edge_stack = initialize_edge_stack();

    //while there are edges in stack
    while(edge_stack.size() > 0) {

        //take the edge on top of stack
        Edge *e = edge_stack.top();

        //unmark it
        e -> mark = false;

        //remove the edge from top of stack
        edge_stack.pop();
  
        //if edge has two neighboring triangles
        if(e->triangles.size() == 2) {
            
            //check if edge violates Delaunay constraints
            bool violates = check_for_edge_violation(e);

            if(violates) {
                //do the edge swap for both of the neighboring triangles of the original edge
                replace_triangle(e->triangles[0], e->triangles[1], e, e->p1);
                replace_triangle(e->triangles[1], e->triangles[0], e, e->p2);
                
                //push the unmarked edges of two neighboring triangles to stack
                push_unmarked_edges(e->triangles[0], e->triangles[1], e, e->p1, edge_stack);
                push_unmarked_edges(e->triangles[1], e->triangles[0], e, e->p2, edge_stack);
                
                //update the neighboring triangles for all the edges
                update_triangle_edges();
            }
        }
    }
}

int main(int argc, char **argv) {
    
    if(argc < 2) {
        cout << "Usage: delaunay <points>";
        return 1;
    }

    //load point data from csv file
    load_points_from_file(argv[1]);

    //sort the loaded coordinates
    sort(coords.begin(), coords.end());

    //construct the inital triangulation
    do_basic_triangulation();

    //store the initial triangulation into file
    store_triangles("triangles.txt");

    //construct the edge data from triangulation
    compute_edges();

    //run the edge flip algorithm
    do_edge_flip();

    //store the Delaunay triangulation to file
    store_triangles("triangles2.txt");

    //store the sorted points to file
    store_points("points2.txt");

    return 0;
}