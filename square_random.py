from delaunay import Delaunay
import numpy
import datetime

# compute mesh of 10 points
delaunay = Delaunay()
for i in range(100):
    print("Adding " + str(i))
    delaunay.addPoint(numpy.random.uniform(-0.2,0.2,2))    
t1 = datetime.datetime.now()
delaunay.edgesSwap()
t2 = datetime.datetime.now()
print(t2-t1)
delaunay.printMatlabScript()